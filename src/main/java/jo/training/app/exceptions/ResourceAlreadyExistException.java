package jo.training.app.exceptions;

public class ResourceAlreadyExistException extends RuntimeException {

  public ResourceAlreadyExistException(final String message) {
    super(message);
  }
}
