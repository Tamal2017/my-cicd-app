package jo.training.app.services.impl;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import jo.training.app.entities.UserEntity;
import jo.training.app.exceptions.ResourceAlreadyExistException;
import jo.training.app.exceptions.UserNotFoundException;
import jo.training.app.mappers.UserMapper;
import jo.training.app.models.User;
import jo.training.app.repositories.UserRepository;
import jo.training.app.services.UserService;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
public class UserServiceImpl implements UserService {

  static final String USER_NOT_FOUND_MESSAGE = "User not found with id : {0}";
  static final String USER_ALREADY_EXIST_MESSAGE = "User already exist with first name : {0}";
  final UserRepository userRepository;
  final UserMapper userMapper;

  @Override
  public User createUser(User user) {
    if (userRepository.existsUserEntityByFirstNameIgnoreCase(user.getFirstName())) {
      throw new ResourceAlreadyExistException(
          MessageFormat.format(USER_ALREADY_EXIST_MESSAGE, user.getFirstName()));
    }

    /* Getting related Entity */
    UserEntity userEntity = userMapper.asUserEntity(user);

    User createdUser = userMapper.asUser(userRepository.save(userEntity));

    log.info("createUser end ok - {} : ", createdUser);
    log.trace("createUser end ok - {} : ", createdUser);

    return createdUser;
  }

  @Override
  public User updateUser(User user) {
    if (!userRepository.existsById(user.getUserId())) {
      throw new UserNotFoundException(
          MessageFormat.format(USER_NOT_FOUND_MESSAGE, user.getUserId()));
    }
    /* Getting related Entity */
    UserEntity userEntity = userMapper.asUserEntity(user);

    User updatedUser = userMapper.asUser(userRepository.save(userEntity));

    log.info("updateUser end ok - {} : ", updatedUser);
    log.trace("updateUser end ok - {} : ", updatedUser);

    return updatedUser;
  }

  @Override
  public User deleteUser(Long userId) {

    /* Getting user by id */
    User deletedUser = userMapper
        .asUser(userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(
            MessageFormat.format(USER_NOT_FOUND_MESSAGE, userId))));

    /* Deleting entity */
    userRepository.deleteById(userId);

    log.info("deleteUser end ok - {} : ", deletedUser);
    log.trace("deleteUser end ok - {} : ", deletedUser);

    return deletedUser;
  }

  @Override
  public List<User> readAllUsers() {
    List<User> users = new ArrayList<>();

    userRepository.findAll().forEach(user -> users.add(userMapper.asUser(user)));

    log.info("readAllUsers end ok - {} : ", users);
    log.trace("readAllUsers end ok - {} : ", users);

    return users;
  }

  @Override
  public User readUserById(Long userId) {
    /* Getting user by id */
    User user = userMapper
        .asUser(userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(
            MessageFormat.format(USER_NOT_FOUND_MESSAGE, userId))));

    log.info("readUserById end ok - {} : ", user);
    log.trace("readUserById end ok - {} : ", user);

    return user;
  }

}
