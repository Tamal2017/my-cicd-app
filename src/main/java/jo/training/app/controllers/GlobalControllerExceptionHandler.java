package jo.training.app.controllers;

import java.util.List;
import java.util.stream.Collectors;
import jo.training.app.exceptions.ResourceAlreadyExistException;
import jo.training.app.exceptions.UserNotFoundException;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
@RestControllerAdvice
public class GlobalControllerExceptionHandler {

  @ResponseStatus(value = HttpStatus.NOT_FOUND)
  @ExceptionHandler(UserNotFoundException.class)
  public Error notFound(final UserNotFoundException e){
    log.error(e.getMessage());

    final Error error = new Error();
    error.setCode(HttpStatus.NOT_FOUND.value());
    error.setMessage(e.getMessage());

    return error;
  }

  @ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
  @ExceptionHandler(MethodArgumentNotValidException.class)
  public Error invalidInput(final MethodArgumentNotValidException e) {

    log.error(e.getMessage());

    final Error error = new Error();
    error.setCode(HttpStatus.UNPROCESSABLE_ENTITY.value());
    error.setMessage("Unable to process data");
    error.setErrors(
        e.getBindingResult().getFieldErrors().stream().map(objectError -> objectError.getField() + " " + objectError.getDefaultMessage()).collect(
            Collectors.toList()));

    return error;
  }

  @ResponseStatus(value = HttpStatus.CONFLICT)
  @ExceptionHandler(ResourceAlreadyExistException.class)
  public Error conflict(final ResourceAlreadyExistException e) {

    log.error(e.getMessage());

    final Error error = new Error();
    error.setCode(HttpStatus.CONFLICT.value());
    error.setMessage(e.getMessage());

    return error;
  }

  @Data
  class Error {
    private Integer code;
    private String message;
    private List<String> errors;
  }
}
