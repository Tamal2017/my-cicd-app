package jo.training.app.services;

import java.util.List;
import jo.training.app.models.User;

public interface UserService {

  User createUser(User user);

  User updateUser(User user);

  User deleteUser(Long userId);

  List<User> readAllUsers();

  User readUserById(Long userId);
}
