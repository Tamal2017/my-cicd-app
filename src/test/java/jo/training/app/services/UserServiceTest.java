package jo.training.app.services;


import java.util.List;
import jo.training.app.exceptions.UserNotFoundException;
import jo.training.app.models.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@Transactional
public class UserServiceTest {

  @Autowired
  private UserService userService;

  @Test
  public void createUser() {
    /* Init data */
    User user = User.builder().firstName("test").lastName("test").email("mail@mail.com")
        .phone("00000000").address("Anywhere in the world").build();

    /* Creating user */
    User createdUser = userService.createUser(user);
    Assert.assertNotNull("The created user should not be null", createdUser.getUserId());
    Assert.assertEquals(
        String.format("The first name of created user  should be : %s", user.getFirstName()),
        user.getFirstName(), createdUser.getFirstName());
    Assert.assertEquals(
        String.format("The last name of created user  should be : %s", user.getLastName()),
        user.getFirstName(), createdUser.getLastName());
    Assert.assertEquals(
        String.format("The address of created user  should be : %s", user.getAddress()),
        user.getAddress(), createdUser.getAddress());
    Assert.assertEquals(
        String.format("The phone of created user  should be : %s", user.getPhone()),
        user.getPhone(), createdUser.getPhone());
  }

  @Test
  public void updateUser() {
    User user = userService.readUserById(1l);
    Assert.assertNotNull(user);

    /* Modifying properties */
    user.setFirstName("updated");
    user.setPhone("1111111");

    /* Updating user */
    User updatedUser = userService.updateUser(user);

    Assert.assertEquals(
        String.format("The first name of updated user  not be : %s", user.getFirstName()),
        user.getFirstName(), updatedUser.getFirstName());
    Assert.assertEquals(
        String.format("The phone number of updated user  not be : %s", user.getPhone()),
        user.getPhone(), updatedUser.getPhone());
  }

  @Test
  public void readAllUsers() {
    List<User> users = userService.readAllUsers();

    Assert.assertNotNull("User list should not be null", users);
    Assert.assertFalse("User list should contain at least one element", users.isEmpty());
  }

  @Test(expected = UserNotFoundException.class)
  public void deleteUser() {
    User user = userService.readUserById(2l);
    Assert.assertNotNull(user);

    /* Deleting user */
    userService.deleteUser(2l);

    /* Testing deleting */
    userService.readUserById(2l);
  }
}