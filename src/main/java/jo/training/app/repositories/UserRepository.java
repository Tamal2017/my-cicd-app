package jo.training.app.repositories;

import jo.training.app.entities.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<UserEntity, Long> {

  boolean existsUserEntityByFirstNameIgnoreCase(String firstName);
}
