package jo.training.app;

import jo.training.app.models.User;
import jo.training.app.services.UserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

/**
 * Init class, launcher
 */
@SpringBootApplication(scanBasePackages = "jo.training.app")
public class MyCicdAppApplication {

  public static void main(String[] args) {
    SpringApplication.run(MyCicdAppApplication.class, args);
  }

  @Bean
  public CommandLineRunner initUsers(UserService userService) {
    return args -> {
      User user = User.builder().firstName("Joseph").lastName("TAMO").email("mail@mail.com")
          .phone("00000000").address("101 roadway street").build();
      userService.createUser(user);
    };
  }

}
