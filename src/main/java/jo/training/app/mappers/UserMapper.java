package jo.training.app.mappers;

import jo.training.app.entities.UserEntity;
import jo.training.app.models.User;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface UserMapper {

  User asUser(UserEntity userEntity);

  UserEntity asUserEntity(User user);
}
