package jo.training.app.controllers;

import java.util.List;
import javax.validation.Valid;
import jo.training.app.models.User;
import jo.training.app.services.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping("/v1")
@Validated
public class UserController {

  private final UserService userService;

  @PostMapping(value = "/users", consumes = "application/json")
  @ResponseStatus(HttpStatus.CREATED)
  public User createUser(@RequestBody @Valid User user) {
    return userService.createUser(user);
  }

  @PutMapping(value = "/users/{userId}", consumes = "application/json")
  @ResponseStatus(HttpStatus.CREATED)
  public User updateUser(@RequestBody @Valid User user, @PathVariable Long userId) {
    /* Setting user id */
    user.setUserId(userId);

    /* Updating user */
    return userService.updateUser(user);
  }

  @GetMapping(value = "/users", produces = "application/json")
  @ResponseStatus(HttpStatus.OK)
  public List<User> readAllUsers() {
    return userService.readAllUsers();
  }

  @GetMapping(value = "/users/{userId}", produces = "application/json")
  @ResponseStatus(HttpStatus.OK)
  public User readUser(@PathVariable("userId") Long userId) {
    return userService.readUserById(userId);
  }

  @DeleteMapping(value = "/users/{userId}", consumes = "application/json")
  @ResponseStatus(HttpStatus.OK)
  public User deleteUser(@PathVariable("userId") Long userId) {
    return userService.deleteUser(userId);
  }

}
